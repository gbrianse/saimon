class Predictor():
    def __init__(self, model, columns=None):
        # init
        self.model = model
        self.name = model.__class__.__name__
        self.columns = columns
    
    def predict(self, df):
        if not self.columns is None:
            df = df[self.columns]
        return self.model.predict(df)
    
    def explain(self):
        tpl = "Model: %s\r\n" +\
              "Columns: %s"
        return tpl % (self.name, ",".join(self.columns))