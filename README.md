# saimon

![SAIMON][logo] 

*S.A.I.M.ON* is a proposal to run a challenge based on using ML/AI to solve business problems.

# How it works
Particpants will be presented with a business problem suitabe to be solved using ML/AI algorithms.
For each challenge there will be two a data set available for trating and testing. 

## Dataset
A set of features to build the model. 

Participants can use all of them, a subset, add new ones based on those features, transform their values...   

### Testing
Participants must keep part of the dataset to validate their algorithms.

# Participate
To participate just clone this repo and for each of the challenges you want to join, create a folder at challenge's folder named as the participant.

The folder will contain up to three copies of jupyter file `challengename_master.ipynb` named `challendername_version.ipynb` where **challengername** is the name of the challenge and **version** is a number.

Example:
````
challenge_test
    challenge_test_master.ipynb
    scores.md
    participant1
        challenge_test_1.ipynb
        challenge_test_2.ipynb
    participant2
        challenge_test_1.ipynb
```` 

# Acceptance

#### Only the latest 3 documents will be evaluated.

The algorithm must achieve a mimum of 70% accuracy.
All accepted algorithms will be proved gainst a final dataset to score them.

# Results
For each challenge a `scores.md` file will show all scores during the challenge.
The winner will be post at the section **Sorcerer** at the end of the challenge.


[logo]: resources/saimon_logo.png "s.a.i.m.o.n : Sembo Artificial Intelligence Master ON"
